<?php

function bem_admin_settings() {
  $form = array();

  $form['bem_replace'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replace classes with BEM'),
    '#description' => t('Replace entity/field classes with BEM classes, instead of append. Default disabled.'),
    '#default_value' => variable_get('bem_replace'),
  );

  foreach (taxonomy_get_vocabularies() as $vid => $vocab) {
    $options[$vocab->machine_name] = $vocab->name;
  }
  $form['bem_vocabs'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Vocabularies to use as modifers for entities'),
    '#description' => t('BEM classes for entities will use modifiers for tagged terms in these vocabularies.'),
    '#default_value' => variable_get('bem_vocabs', array()),
    '#options' => $options,
  );

  $form['#submit'][] = 'bem_admin_settings_submit';
  return system_settings_form($form, TRUE);
}

// Custom submit
function bem_admin_settings_submit($form, &$form_state) {
  // Clear bem
  variable_set('bem', NULL);
}
