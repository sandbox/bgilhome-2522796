<?php // Only prints wrappers if there are classes/attributes defined for them ?>
<?php if ($classes || $attributes): ?>
  <div class="<?php print $classes; ?>"<?php print $attributes; ?>>
<?php endif; ?>

<?php if (!$label_hidden): ?>

  <?php if ($title_attributes): ?>
    <div <?php print $title_attributes; ?>>
  <?php endif; ?>

  <?php print $label ?>:&nbsp;

  <?php if ($title_attributes): ?>
    </div>
  <?php endif; ?>

<?php endif; ?>

<?php if ($content_attributes): ?>
  <div <?php print $content_attributes; ?>>
<?php endif; ?>

<?php foreach ($items as $delta => $item): ?>

  <?php if ($item_attributes[$delta]): ?>
    <div <?php print $item_attributes[$delta]; ?>>
  <?php endif; ?>

  <?php print render($item); ?>

  <?php if ($item_attributes[$delta]): ?>
    </div>
  <?php endif; ?>

<?php endforeach; ?>

<?php if ($content_attributes): ?>
  </div>
<?php endif; ?>

<?php if ($classes || $attributes): ?>
  </div>
<?php endif; ?>
