<?php // Only prints wrappers if there are classes/attributes defined for them ?>
<?php if ($classes || $attributes): ?>
  <div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<?php endif; ?>

<?php if (!$page): ?>
  <h2<?php print $title_attributes; ?>>
    <?php if ($url): ?>
      <a href="<?php print $url; ?>"><?php print $title; ?></a>
    <?php else: ?>
      <?php print $title; ?>
    <?php endif; ?>
  </h2>
<?php endif; ?>

<?php if ($content_attributes): ?>
  <div <?php print $content_attributes; ?>>
<?php endif; ?>

<?php print render($content); ?>

<?php if ($content_attributes): ?>
  </div>
<?php endif; ?>

<?php if ($classes || $attributes): ?>
  </div>
<?php endif; ?>
