<?php

/**
 * Implements hook_bem_alter().
 */
function hook_bem_alter(&$bem) {
  // Give article a BEM class of story
  $bem['node']['article']['block'] = 'story';
  // Give article body a BEM class of story__blurb
  $bem['node']['article']['elements']['body'] = 'blurb';
  // Give article teasers a BEM class of story--short
  $bem['node']['article']['modifiers']['teaser'] = 'short';
}
